package com.icollect.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.icollect.client.adapter.DetailStoresRecyclerViewAdapter;
import com.icollect.client.adapter.DetailTicketsRecyclerViewAdapter;
import com.icollect.client.api.HttpRequest;
import com.icollect.client.entity.ActivityDescription;
import com.icollect.client.entity.Store;
import com.icollect.client.entity.Ticket;
import com.icollect.client.events.LeaveActivityEvent;
import com.icollect.client.events.WatchingActivityEvent;
import com.icollect.client.helpers.NetworkDetector;
import com.icollect.client.itemanimator.CustomItemAnimator;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class DetailActivity extends AppCompatActivity
{
    SharedPreferences sp;
    String mSession;
    ImageView bannerParallaxImage;
    ActivityDescription activityDescription;
    RecyclerView ticketList;
    RecyclerView storeList;
    DetailTicketsRecyclerViewAdapter ticketsRecyclerViewAdapter;
    DetailStoresRecyclerViewAdapter storesRecyclerViewAdapter;
    DisplayMetrics displayMetrics;
    TextView ticketsHeader;
    TextView storesHeader;
    TextView gainStartDate;
    TextView gainEndDate;
    TextView exStartDate;
    TextView exEndDate;
    int activityId;
    FloatingActionButton favoriteBtn;
    boolean isFavorite;

    @Override
    protected void onResume()
    {
        super.onResume();
        EventBus.getDefault().post(new WatchingActivityEvent(activityId));
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        EventBus.getDefault().post(new LeaveActivityEvent(activityId));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        mSession = sp.getString("session", null);

        Bundle extras = getIntent().getExtras();
        activityDescription = new Gson().fromJson(extras.getString("activity"), ActivityDescription.class);
        activityId = activityDescription.getId();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        TextView activityBodyTextView = (TextView) findViewById(R.id.activity_body_textview);
        collapsingToolbar.setTitle(activityDescription.getActivityName());
        bannerParallaxImage = (ImageView) findViewById(R.id.banner_parallax_image);
        ImageLoader.getInstance().displayImage(activityDescription.getActivityImageURL(), bannerParallaxImage);
        setSupportActionBar(toolbar);

        activityBodyTextView.setText(activityDescription.getActivityContent());

        ticketsHeader = (TextView) findViewById(R.id.tickets_header);
        storesHeader = (TextView) findViewById(R.id.stores_header);

        ticketsHeader.setVisibility(View.GONE);
        storesHeader.setVisibility(View.GONE);

        gainStartDate = (TextView) findViewById(R.id.gain_start_date);
        gainEndDate = (TextView) findViewById(R.id.gain_end_date);
        exStartDate = (TextView) findViewById(R.id.ex_start_date);
        exEndDate = (TextView) findViewById(R.id.ex_end_date);

        gainStartDate.setText(activityDescription.getGainStartDate());
        gainEndDate.setText(activityDescription.getGainEndDate());
        exStartDate.setText(activityDescription.getExchangeStartDate());
        exEndDate.setText(activityDescription.getExchangeEndDate());

        ticketList = (RecyclerView) findViewById(R.id.detail_ticket_list);
        storeList = (RecyclerView) findViewById(R.id.detail_store_list);

        ticketsRecyclerViewAdapter = new DetailTicketsRecyclerViewAdapter(new ArrayList<Ticket>(), R.layout.partial_activities_detail_tickets_row, this);
        storesRecyclerViewAdapter = new DetailStoresRecyclerViewAdapter(new ArrayList<Store>(), R.layout.partial_activities_detail_stores_row, this);

        ticketList.setAdapter(ticketsRecyclerViewAdapter);
        ticketList.setLayoutManager(new LinearLayoutManager(this));
        ticketList.setItemAnimator(new CustomItemAnimator());
        ticketList.setHasFixedSize(false);
        ticketList.setNestedScrollingEnabled(false);

        storeList.setAdapter(storesRecyclerViewAdapter);
        storeList.setLayoutManager(new LinearLayoutManager(this));
        storeList.setItemAnimator(new CustomItemAnimator());
        storeList.setHasFixedSize(false);
        storeList.setNestedScrollingEnabled(false);

        displayMetrics = getResources().getDisplayMetrics();
        new GetTicketsOfActivityAsyncTask().execute(activityId);
        new GetStoresOfActivityAsyncTask().execute(activityId);
        new IsFavoriteActivityAsyncTask().execute(activityId);

        isFavorite = false;
        favoriteBtn = (FloatingActionButton) findViewById(R.id.favorite_button);
        favoriteBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (isFavorite) {
                    removeFromFavorite();
                } else {
                    addToFavorite();
                }
            }
        });
    }

    public void setupFavoriteButton()
    {
        if (isFavorite) {
            favoriteBtn.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
            favoriteBtn.setImageDrawable(ContextCompat.getDrawable(DetailActivity.this, R.drawable.ic_check_light_green_24dp));
        } else {
            favoriteBtn.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(DetailActivity.this, R.color.pink_pressed)));
            favoriteBtn.setImageDrawable(ContextCompat.getDrawable(DetailActivity.this, R.drawable.ic_favorite_white_24dp));
        }
    }

    public void addToFavorite()
    {
        if (NetworkDetector.getInstance().isNetworkAvailable(DetailActivity.this)) {
            new AddToFavoriteActivityAsyncTask().execute(activityId);
        } else {
            showNoNetworkMessage();
        }
    }

    public void removeFromFavorite()
    {
        if (NetworkDetector.getInstance().isNetworkAvailable(DetailActivity.this)) {
            new RemoveFromFavoriteActivityAsyncTask().execute(activityId);
        } else {
            showNoNetworkMessage();
        }
    }

    public void showNoNetworkMessage()
    {
        SnackbarManager.show(
                Snackbar.with(getApplicationContext())
                        .type(SnackbarType.SINGLE_LINE)
                        .position(Snackbar.SnackbarPosition.BOTTOM)
                        .text("沒有網路連線，請確保網路連線狀況")
                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                        .animation(true)
                , DetailActivity.this);
    }

    class RemoveFromFavoriteActivityAsyncTask extends AsyncTask<Integer, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Integer... integers)
        {
            int activityId = integers[0];
            HttpRequest request = HttpRequest.delete(getString(R.string.remove_from_favorite_activity) + activityId).header("Cookie", mSession);
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONObject o = new JSONObject(response);
                    boolean status = o.getBoolean("status");
                    return status;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean status)
        {
            if (status) {
                favoriteBtn.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(DetailActivity.this, R.color.pink_pressed)));
                favoriteBtn.setImageDrawable(ContextCompat.getDrawable(DetailActivity.this, R.drawable.ic_favorite_white_24dp));
                isFavorite = false;
            }
        }
    }

    class AddToFavoriteActivityAsyncTask extends AsyncTask<Integer, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Integer... integers)
        {
            int activityId = integers[0];
            HttpRequest request = HttpRequest.post(getString(R.string.add_to_favorite_activity) + activityId).header("Cookie", mSession);
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONObject o = new JSONObject(response);
                    boolean status = o.getBoolean("status");
                    return status;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean status)
        {
            if (status) {
                favoriteBtn.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
                favoriteBtn.setImageDrawable(ContextCompat.getDrawable(DetailActivity.this, R.drawable.ic_check_light_green_24dp));
                isFavorite = true;
            }
        }
    }

    class IsFavoriteActivityAsyncTask extends AsyncTask<Integer, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Integer... integers)
        {
            int activityId = integers[0];
            HttpRequest request = HttpRequest.get(getString(R.string.is_favorite_activity) + activityId).header("Cookie", mSession);
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONObject o = new JSONObject(response);
                    boolean status = o.getBoolean("status");
                    if (status) {
                        boolean isFavorite = o.getBoolean("is_favorite");
                        return isFavorite;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean isFavoriteActivity)
        {
            isFavorite = isFavoriteActivity;
            setupFavoriteButton();
        }
    }

    class GetTicketsOfActivityAsyncTask extends AsyncTask<Integer, Void, Void>
    {
        ArrayList<Ticket> tickets = new ArrayList<>();

        @Override
        protected Void doInBackground(Integer... params)
        {
            int aid = params[0];

            HttpRequest request = HttpRequest.get(getString(R.string.get_tickets_of_activity) + aid);
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONArray ticketsArray = new JSONArray(response);
                    for (int i = 0; i < ticketsArray.length(); i++) {
                        Ticket ticket = new Ticket();
                        JSONObject ticketJson = ticketsArray.getJSONObject(i);
                        String name = ticketJson.getString("name");
                        String body = ticketJson.getString("body");
                        int price = ticketJson.getInt("threshold");
                        String image = ticketJson.getString("image");
                        ticket.setName(name);
                        ticket.setBody(body);
                        ticket.setPrice(price);
                        ticket.setImage(getString(R.string.icollect_domain) + "/" + image);
                        tickets.add(ticket);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            ticketsRecyclerViewAdapter.addTickets(tickets);
            float dpHeight = 205 * displayMetrics.density;
            ticketList.setMinimumHeight((int) (tickets.size() * dpHeight));

            if (tickets.size() > 0) {
                ticketsHeader.setVisibility(View.VISIBLE);
            }
        }
    }

    class GetStoresOfActivityAsyncTask extends AsyncTask<Integer, Void, Void>
    {
        ArrayList<Store> stores;

        @Override
        protected Void doInBackground(Integer... params)
        {
            int aid = params[0];
            HttpRequest request = HttpRequest.get(getString(R.string.get_stores_of_activity) + aid);

            if (request.ok()) {
                String response = request.body();
                try {
                    JSONArray storesJson = new JSONArray(response);
                    stores = new ArrayList<>();
                    for (int i = 0; i < storesJson.length(); i++) {
                        Store store = new Store();
                        JSONObject o = storesJson.getJSONObject(i);
                        String name = o.getString("name");
                        String address = o.getString("address");
                        String tel = o.getString("tel");
                        store.setName(name);
                        store.setAddress(address);
                        store.setTel(tel);
                        stores.add(store);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            storesRecyclerViewAdapter.addStores(stores);
            float dpHeight = 72 * displayMetrics.density;
            storeList.setMinimumHeight((int) (stores.size() * dpHeight));

            if (stores.size() > 0) {
                storesHeader.setVisibility(View.VISIBLE);
            }
        }
    }
}
