package com.icollect.client.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.icollect.client.R;
import com.icollect.client.adapter.AllFragmentRecyclerViewAdapter;
import com.icollect.client.api.HttpRequest;
import com.icollect.client.entity.ActivityDescription;
import com.icollect.client.itemanimator.CustomItemAnimator;
import com.icollect.client.listeners.EndlessScrollRecyclerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FavoriteActivitiesFragment extends Fragment
{
    SharedPreferences sp;
    String mSession;
    String icollectDomain;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    AllFragmentRecyclerViewAdapter allFragmentRecycleViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.fragment_all_activity, container, false);
        sp = getActivity().getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        mSession = sp.getString("session", null);
        icollectDomain = getString(R.string.icollect_domain);
        progressBar = (ProgressBar) layout.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) layout.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new CustomItemAnimator());
        allFragmentRecycleViewAdapter = new AllFragmentRecyclerViewAdapter(new ArrayList<ActivityDescription>(), R.layout.partial_fragment_all_activity_row, getActivity());
        recyclerView.setAdapter(allFragmentRecycleViewAdapter);

        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.theme_accent));
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                allFragmentRecycleViewAdapter.clearActivities();
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                new GetFavoriteActivitiesAsyncTask().execute();
            }
        });
        allFragmentRecycleViewAdapter.clearActivities();
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        new GetFavoriteActivitiesAsyncTask().execute();
        return layout;
    }

    private class GetFavoriteActivitiesAsyncTask extends AsyncTask<Void, Void, Void>
    {
        List<ActivityDescription> favoriteActivityDescriptionList = new ArrayList<>();
        @Override
        protected Void doInBackground(Void... params)
        {
            HttpRequest request = HttpRequest.get(getActivity().getString(R.string.get_favorite_activities)).header("Cookie", mSession);
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        int id = object.getInt("id");
                        String name = object.getString("name");
                        String body = object.getString("body");
                        String imageURL = icollectDomain + "/" + object.getString("image");
                        String gainStartDate = object.getString("gain_start");
                        String gainEndDate = object.getString("gain_end");
                        String exchangeStartDate = object.getString("ex_start");
                        String exchangeEndDate = object.getString("ex_end");

                        ActivityDescription activityDescription = new ActivityDescription();
                        activityDescription.setId(id);
                        activityDescription.setActivityName(name);
                        activityDescription.setActivityContent(body);
                        activityDescription.setActivityImageURL(imageURL);
                        activityDescription.setGainStartDate(gainStartDate);
                        activityDescription.setGainEndDate(gainEndDate);
                        activityDescription.setExchangeStartDate(exchangeStartDate);
                        activityDescription.setExchangeEndDate(exchangeEndDate);
                        favoriteActivityDescriptionList.add(activityDescription);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            recyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            allFragmentRecycleViewAdapter.addActivities(favoriteActivityDescriptionList);
        }
    }
}
