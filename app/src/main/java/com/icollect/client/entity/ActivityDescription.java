package com.icollect.client.entity;

/**
 * Created by Administrator on 2015/7/12.
 */
public class ActivityDescription
{
    private int id;
    private String activityImageURL;
    private String activityName;
    private String activityContent;
    private String gainStartDate;
    private String gainEndDate;
    private String exchangeStartDate;
    private String exchangeEndDate;

    public String getExchangeEndDate()
    {
        return exchangeEndDate;
    }

    public void setExchangeEndDate(String exchangeEndDate)
    {
        this.exchangeEndDate = exchangeEndDate;
    }

    public String getExchangeStartDate()
    {
        return exchangeStartDate;
    }

    public void setExchangeStartDate(String exchangeStartDate)
    {
        this.exchangeStartDate = exchangeStartDate;
    }

    public String getGainEndDate()
    {
        return gainEndDate;
    }

    public void setGainEndDate(String gainEndDate)
    {
        this.gainEndDate = gainEndDate;
    }

    public String getGainStartDate()
    {
        return gainStartDate;
    }

    public void setGainStartDate(String gainStartDate)
    {
        this.gainStartDate = gainStartDate;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getActivityContent()
    {
        return activityContent;
    }

    public void setActivityContent(String activityContent)
    {
        this.activityContent = activityContent;
    }

    public String getActivityName()
    {
        return activityName;
    }

    public void setActivityName(String activityName)
    {
        this.activityName = activityName;
    }

    public String getActivityImageURL()
    {
        return activityImageURL;
    }

    public void setActivityImageURL(String activityImageURL)
    {
        this.activityImageURL = activityImageURL;
    }
}
