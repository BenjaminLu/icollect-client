package com.icollect.client.entity;

import java.util.List;

/**
 * Created by Administrator on 2015/9/26.
 */
public class UsedTicketList
{
    public List<Integer> ticketIds;

    public List<Integer> getTicketIds()
    {
        return ticketIds;
    }

    public void setTicketIds(List<Integer> ticketIds)
    {
        this.ticketIds = ticketIds;
    }
}
