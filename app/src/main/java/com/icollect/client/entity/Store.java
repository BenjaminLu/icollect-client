package com.icollect.client.entity;

/**
 * Created by Administrator on 2015/10/18.
 */
public class Store
{
    String name;
    String address;
    String tel;

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTel()
    {
        return tel;
    }

    public void setTel(String tel)
    {
        this.tel = tel;
    }
}
