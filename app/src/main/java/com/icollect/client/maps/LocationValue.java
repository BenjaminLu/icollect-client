package com.icollect.client.maps;

/**
 * Created by Administrator on 2015/10/13.
 */
public class LocationValue
{
    double longitude;
    double latitude;

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    @Override
    public String toString()
    {
        return "Latitude : " + latitude + " Longitude : " + longitude;
    }
}
