package com.icollect.client.helpers;

/**
 * Created by Administrator on 2015/10/21.
 */
public class UserIdHelper
{
    private int userId;
    private static UserIdHelper ourInstance = new UserIdHelper();

    public static UserIdHelper getInstance()
    {
        return ourInstance;
    }

    private UserIdHelper()
    {
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }
}
