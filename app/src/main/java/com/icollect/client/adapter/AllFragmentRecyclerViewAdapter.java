package com.icollect.client.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.icollect.client.DetailActivity;
import com.icollect.client.R;
import com.icollect.client.entity.ActivityDescription;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class AllFragmentRecyclerViewAdapter
        extends RecyclerView.Adapter<AllFragmentRecyclerViewAdapter.ViewHolder>
{
    private List<ActivityDescription> activities;
    private int allActivityRowLayout;
    private Activity mainActivity;

    public AllFragmentRecyclerViewAdapter(List<ActivityDescription> activities, int allActivityRowLayout, Activity mainActivity)
    {
        this.activities = activities;
        this.allActivityRowLayout = allActivityRowLayout;
        this.mainActivity = mainActivity;
    }

    public void clearActivities()
    {
        int size = this.activities.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                activities.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addActivities(List<ActivityDescription> applications)
    {
        int lastSize = activities.size();
        this.activities.addAll(applications);
        this.notifyItemRangeInserted(lastSize, applications.size() - 1);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i)
    {
        View allActivityRowLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(allActivityRowLayout, viewGroup, false);
        return new ViewHolder(allActivityRowLayoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i)
    {
        final ActivityDescription activityDescription = activities.get(i);
        viewHolder.activityName.setText(activityDescription.getActivityName());
        ImageLoader.getInstance().displayImage(activityDescription.getActivityImageURL(), viewHolder.image);

        final View image = viewHolder.image;
        viewHolder.cardLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(mainActivity, DetailActivity.class);
                Bundle b = new Bundle();
                b.putString("activity", new Gson().toJson(activityDescription));
                i.putExtras(b);
                mainActivity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return activities == null ? 0 : activities.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView activityName;
        public ImageView image;
        public LinearLayout cardLayout;

        public ViewHolder(View allActivityRowLayoutView)
        {
            super(allActivityRowLayoutView);
            activityName = (TextView) allActivityRowLayoutView.findViewById(R.id.activityName);
            image = (ImageView) allActivityRowLayoutView.findViewById(R.id.banner_image);
            cardLayout = (LinearLayout) allActivityRowLayoutView.findViewById(R.id.card_linear_layout);
        }

    }
}
