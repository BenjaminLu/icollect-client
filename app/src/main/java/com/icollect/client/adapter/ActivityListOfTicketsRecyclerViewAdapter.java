package com.icollect.client.adapter;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.icollect.client.MyTicketActivity;
import com.icollect.client.R;
import com.icollect.client.entity.Ticket;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityListOfTicketsRecyclerViewAdapter
        extends RecyclerView.Adapter<ActivityListOfTicketsRecyclerViewAdapter.ViewHolder>
{
    private List<Ticket> tickets;
    private int activityOfTicketsRowLayout;
    private MyTicketActivity activity;
    ArrayList<Integer> usedTicketIds = new ArrayList<>();

    public ActivityListOfTicketsRecyclerViewAdapter(ArrayList<Ticket> tickets, int activityOfTicketsRowLayout, MyTicketActivity a)
    {
        this.tickets = tickets;
        this.activityOfTicketsRowLayout = activityOfTicketsRowLayout;
        this.activity = a;
    }

    public void clearTickets()
    {
        if (tickets != null) {
            int size = this.tickets.size();
            tickets.clear();
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addTickets(ArrayList<Ticket> newTickets)
    {
        if (tickets != null) {
            int lastSize = tickets.size();
            this.tickets.addAll(newTickets);
            this.notifyItemRangeInserted(lastSize, newTickets.size());
        }
    }

    public void setUsedTicketIds(List<Integer> ticketIds)
    {
        usedTicketIds.clear();
        usedTicketIds.addAll(ticketIds);
    }

    public void addUsedTicketId(Integer usedTicketId)
    {
        if (usedTicketIds != null) {
            usedTicketIds.add(usedTicketId);
            ArrayList<Ticket> refreshTickets = cloneTickets(this.tickets);
            clearTickets();
            addTickets(refreshTickets);
        }
    }

    public static ArrayList<Ticket> cloneTickets(List<Ticket> list)
    {
        ArrayList<Ticket> clone = new ArrayList<Ticket>(list.size());
        for (Ticket ticket : list) {
            clone.add(ticket.clone());
        }
        return clone;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i)
    {
        View allActivityRowLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(activityOfTicketsRowLayout, viewGroup, false);
        return new ViewHolder(allActivityRowLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i)
    {
        final Ticket ticket = tickets.get(i);
        ImageLoader.getInstance().displayImage(ticket.getImage(), viewHolder.image);

        viewHolder.ticketName.setText(ticket.getName());
        viewHolder.ticketBody.setText(ticket.getBody());
        viewHolder.ticketPrice.setText(String.valueOf(ticket.getPrice()) + "元");
        viewHolder.useTicketBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                activity.startUseTicketActivity(ticket);
            }
        });

        boolean isUsed = ticket.getStatus();
        for (int index = 0; index < usedTicketIds.size(); index++) {
            int usedId = usedTicketIds.get(index).intValue();
            int tid = ticket.getId();
            if (usedId == tid) {
                isUsed = true;
            }
        }

        String statusString = (isUsed) ? "已使用" : "未使用";
        if (isUsed) {
            viewHolder.swipeLayout.setSwipeEnabled(false);
            viewHolder.ticketStatus.setTextColor(Color.parseColor("#d9534f"));
        } else {
            viewHolder.swipeLayout.setSwipeEnabled(true);
            viewHolder.ticketStatus.setTextColor(Color.parseColor("#5bc0de"));
        }
        viewHolder.ticketStatus.setText(statusString);

        final JSONObject o = new JSONObject();
        try {
            o.put("data", ticket.getData());
            o.put("sign", ticket.getSign());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        viewHolder.card.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(activity).setTitle("票卷資訊").setMessage(o.toString())
                        .setPositiveButton("確定", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        }).create();
                alertDialog.show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return tickets == null ? 0 : tickets.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView image;
        public TextView ticketName;
        public TextView ticketBody;
        public TextView ticketPrice;
        public TextView useTicketBtn;
        public TextView ticketStatus;
        public LinearLayout card;
        public SwipeLayout swipeLayout;

        public ViewHolder(View row)
        {
            super(row);
            image = (ImageView) row.findViewById(R.id.ticket_image);
            ticketName = (TextView) row.findViewById(R.id.ticket_name);
            ticketBody = (TextView) row.findViewById(R.id.ticket_body);
            ticketPrice = (TextView) row.findViewById(R.id.ticket_price);
            useTicketBtn = (TextView) row.findViewById(R.id.use_ticket_btn);
            ticketStatus = (TextView) row.findViewById(R.id.ticket_status);
            card = (LinearLayout) row.findViewById(R.id.ticket_card);
            swipeLayout = (SwipeLayout) row.findViewById(R.id.swipe_layout);
        }
    }
}
