package com.icollect.client;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.icollect.client.api.HttpRequest;
import com.icollect.client.entity.Ticket;
import com.icollect.client.fragment.AllActivitiesFragment;
import com.icollect.client.fragment.FavoriteActivitiesFragment;
import com.icollect.client.helpers.NetworkDetector;
import com.icollect.client.helpers.UserIdHelper;
import com.icollect.client.services.ActivitySubscriberService;
import com.icollect.client.tabs.SlidingTabLayout;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.nostra13.universalimageloader.cache.disc.impl.LimitedAgeDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LimitedAgeMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity
{
    TextView userName;
    TextView userEmail;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    SlidingTabLayout slidingTabLayout;
    ViewPager viewPager;
    SharedPreferences sp;
    String mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setup universal image loader
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.image_loading)
                .resetViewBeforeLoading(false)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .build();
        File cacheDir = StorageUtils.getCacheDirectory(getApplicationContext());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .taskExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                .taskExecutorForCachedImages(AsyncTask.THREAD_POOL_EXECUTOR)
                .threadPoolSize(6)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LimitedAgeMemoryCache(new LruMemoryCache(100 * 1024 * 1024), 15 * 60))
                .diskCache(new LimitedAgeDiskCache(cacheDir, 15 * 60))
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .imageDownloader(new BaseImageDownloader(getApplicationContext()))
                .defaultDisplayImageOptions(options)
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);

        sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        mSession = sp.getString("session", null);

        userName = (TextView) findViewById(R.id.user_name_textview);
        userEmail = (TextView) findViewById(R.id.user_email_textview);
        boolean isNetworkAvailable = NetworkDetector.getInstance().isNetworkAvailable(MainActivity.this);
        if (isNetworkAvailable) {
            viewPager = (ViewPager) findViewById(R.id.viewpager);
            viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
            slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs_layout);
            slidingTabLayout.setViewPager(viewPager);
            MainTabColorizer mainTabColorizer = new MainTabColorizer();
            slidingTabLayout.setCustomTabColorizer(mainTabColorizer);

            if (mSession != null) {
                new LoginAsyncTask().execute(mSession);
            } else {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        } else {
            userName.setText("尚未登入");
            SnackbarManager.show(
                    Snackbar.with(getApplicationContext())
                            .type(SnackbarType.SINGLE_LINE)
                            .position(Snackbar.SnackbarPosition.BOTTOM)
                            .text("沒有網路連線，請確認網路暢通")
                            .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                            .animation(true)
                    , MainActivity.this);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        actionBarDrawerToggle.syncState();
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        LinearLayout ticketMenuBtn = (LinearLayout) findViewById(R.id.get_ticket_menu_btn);
        View.OnClickListener drawerMenuOnClickListener = new DrawerMenuOnClickListener();
        ticketMenuBtn.setOnClickListener(drawerMenuOnClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_logout :
                if (NetworkDetector.getInstance().isNetworkAvailable(MainActivity.this)) {
                    new LogoutAsyncTask().execute();
                }
                break;
            case R.id.action_search :
                showSearchDialog();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showSearchDialog()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this).setTitle("搜尋活動");
        final EditText editText = new EditText(MainActivity.this);
        alertDialog.setView(editText);
        alertDialog.setNegativeButton("確定", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String activityKeyword = editText.getText().toString();
                dialog.dismiss();
                Intent i = new Intent(MainActivity.this, SearchResultActivity.class);
                Bundle b = new Bundle();
                b.putString("keyword", activityKeyword);
                i.putExtras(b);
                startActivity(i);
            }
        }).create();
        alertDialog.show();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        String[] tabsNames;

        public ViewPagerAdapter(FragmentManager fm)
        {
            super(fm);
            tabsNames = getResources().getStringArray(R.array.tabs_names);
        }

        @Override
        public Fragment getItem(int position)
        {
            Fragment fragment = null;
            switch (position) {
                case 0 :
                    fragment = new AllActivitiesFragment();
                    break;
                case 1 :
                    fragment = new AllActivitiesFragment();
                    break;
                case 2 :
                    fragment = new FavoriteActivitiesFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return tabsNames[position];
        }

        @Override
        public int getCount()
        {
            return tabsNames.length;
        }
    }

    class MainTabColorizer implements SlidingTabLayout.TabColorizer
    {
        String[] indicatorColorStrings;
        int[] indicatorColors;

        MainTabColorizer()
        {
            indicatorColorStrings = getResources().getStringArray(R.array.indicator_colors);
            indicatorColors = new int[indicatorColorStrings.length];
            for (int i = 0; i < indicatorColorStrings.length; i++) {
                indicatorColors[i] = Color.parseColor(indicatorColorStrings[i]);
            }
        }

        @Override
        public int getIndicatorColor(int position)
        {
            return indicatorColors[position];
        }

        @Override
        public int getDividerColor(int position)
        {
            return Color.parseColor(getResources().getString(R.string.indicator_divider_color));
        }
    }

    class DrawerMenuOnClickListener implements View.OnClickListener
    {

        @Override
        public void onClick(View v)
        {
            switch (v.getId()) {
                case R.id.get_ticket_menu_btn:
                    if (NetworkDetector.getInstance().isNetworkAvailable(MainActivity.this)) {
                        Intent myTicketActivity = new Intent(MainActivity.this, MyTicketActivity.class);
                        startActivity(myTicketActivity);
                    }
                    break;
            }
        }
    }

    class LoginAsyncTask extends AsyncTask<String, Void, Boolean>
    {
        JSONObject responseJson;
        String sessionCookie;
        String userNameString;
        String userEmailString;

        @Override
        protected Boolean doInBackground(String... params)
        {
            boolean isSuccessful = false;
            String session = params[0];

            Map<String, String> data = new HashMap<String, String>();
            data.put("email", "anything");
            data.put("password", "anything");
            HttpRequest request = HttpRequest.post(getString(R.string.login_url)).header("Cookie", session).form(data);
            if (request.ok()) {
                String response = request.body();
                try {
                    responseJson = new JSONObject(response);
                    isSuccessful = responseJson.getBoolean("success");
                    sessionCookie = request.header("Set-Cookie");

                    if (isSuccessful) {
                        userNameString = responseJson.getString("name");
                        userEmailString = responseJson.getString("email");
                        int id = responseJson.getInt("id");
                        if (NetworkDetector.getInstance().isNetworkAvailable(MainActivity.this)) {
                            Intent activitySubscriberServiceIntent = new Intent(MainActivity.this, ActivitySubscriberService.class);
                            startService(activitySubscriberServiceIntent);
                        }
                        UserIdHelper.getInstance().setUserId(id);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return isSuccessful;
        }

        @Override
        protected void onPostExecute(Boolean isSuccessful)
        {
            if (isSuccessful.booleanValue()) {
                userName.setText(userNameString);
                userEmail.setText(userEmailString);
                SharedPreferences.Editor editor = sp.edit();
                if (!sessionCookie.equals(mSession)) {
                    editor.clear();
                    editor.putString("session", sessionCookie);
                    editor.commit();
                }
            } else {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }
    }

    class LogoutAsyncTask extends AsyncTask<Void, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Void... params)
        {
            boolean isSuccessful = false;
            HttpRequest request = HttpRequest.get(getString(R.string.logout_url));
            if (request.ok()) {
                String response = request.body();
                JSONObject o;
                try {
                    o = new JSONObject(response);
                    isSuccessful = o.getBoolean("success");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return isSuccessful;
        }

        @Override
        protected void onPostExecute(Boolean isSuccessful)
        {
            if (isSuccessful.booleanValue()) {
                SharedPreferences.Editor editor = sp.edit();
                editor.clear().commit();
                //Logout AsyncTask
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            } else {
                SnackbarManager.show(Snackbar.with(getApplicationContext())
                        .type(SnackbarType.SINGLE_LINE)
                        .text("登出失敗，伺服器連線異常")
                        .duration(Snackbar.SnackbarDuration.LENGTH_LONG)
                        .animation(true), MainActivity.this);
            }
        }
    }
}
