package com.icollect.client;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.icollect.client.api.HttpRequest;
import com.icollect.client.helpers.NetworkDetector;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends AppCompatActivity
{
    private EditText emailET;
    private EditText passwordET;
    private Button loginBtn;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        title = (TextView) findViewById(R.id.title);
        emailET = (EditText) findViewById(R.id.email);
        passwordET = (EditText) findViewById(R.id.password);
        loginBtn = (Button) findViewById(R.id.login_btn);
        loginBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loginBtn.setEnabled(false);

                if (NetworkDetector.getInstance().isNetworkAvailable(LoginActivity.this)) {
                    new LoginAsyncTask().execute(emailET.getText().toString(), passwordET.getText().toString());
                } else {
                    loginBtn.setEnabled(true);
                    SnackbarManager.show(
                            Snackbar.with(getApplicationContext())
                                    .type(SnackbarType.SINGLE_LINE)
                                    .position(Snackbar.SnackbarPosition.BOTTOM)
                                    .text("沒有網路連線，請確認網路暢通")
                                    .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                                    .animation(true)
                            , LoginActivity.this);
                }
            }
        });

        Animation titleAnimation = AnimationUtils.loadAnimation(this, R.anim.login_title_anim);
        Animation controlAnimation = AnimationUtils.loadAnimation(this, R.anim.loggin_control_anim);
        title.startAnimation(titleAnimation);
        emailET.setAnimation(controlAnimation);
        passwordET.setAnimation(controlAnimation);
        loginBtn.setAnimation(controlAnimation);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    class LoginAsyncTask extends AsyncTask<String, Void, Boolean>
    {
        JSONObject responseJson;
        String email;
        String password;
        String sessionCookie;

        @Override
        protected Boolean doInBackground(String... params)
        {
            boolean isSuccessful = false;
            email = params[0];
            password = params[1];

            if (email.equals("") || password.equals("")) {
                return false;
            }

            Map<String, String> data = new HashMap<String, String>();
            data.put("email", email);
            data.put("password", password);
            HttpRequest request = HttpRequest.post(getString(R.string.login_url)).form(data);
            if (request.ok()) {
                String response = request.body();
                this.sessionCookie = request.header("Set-Cookie");
                try {
                    responseJson = new JSONObject(response);
                    isSuccessful = responseJson.getBoolean("success");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return isSuccessful;
        }

        @Override
        protected void onPostExecute(Boolean isSuccessful)
        {
            if (isSuccessful.booleanValue()) {
                SharedPreferences sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.clear();
                editor.putString("session", sessionCookie);
                editor.commit();
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            } else {
                SnackbarManager.show(Snackbar.with(getApplicationContext())
                        .type(SnackbarType.SINGLE_LINE)
                        .text("登入失敗，帳號或密碼錯誤")
                        .duration(Snackbar.SnackbarDuration.LENGTH_LONG)
                        .animation(true), LoginActivity.this);
            }
            loginBtn.setEnabled(true);
        }
    }
}
