package com.icollect.client;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.icollect.client.adapter.AllFragmentRecyclerViewAdapter;
import com.icollect.client.api.HttpRequest;
import com.icollect.client.entity.ActivityDescription;
import com.icollect.client.helpers.NetworkDetector;
import com.icollect.client.itemanimator.CustomItemAnimator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchResultActivity extends AppCompatActivity
{
    RecyclerView searchResultRecyclerView;
    AllFragmentRecyclerViewAdapter allFragmentRecycleViewAdapter;
    private String icollectDomain;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        Bundle b = getIntent().getExtras();
        String keyword = b.getString("keyword");
        icollectDomain = getString(R.string.icollect_domain);
        searchResultRecyclerView = (RecyclerView) findViewById(R.id.search_result_recyclerview);
        searchResultRecyclerView.setLayoutManager(new LinearLayoutManager(SearchResultActivity.this));
        searchResultRecyclerView.setItemAnimator(new CustomItemAnimator());
        allFragmentRecycleViewAdapter = new AllFragmentRecyclerViewAdapter(new ArrayList<ActivityDescription>(), R.layout.partial_fragment_all_activity_row, SearchResultActivity.this);
        searchResultRecyclerView.setAdapter(allFragmentRecycleViewAdapter);

        searchResultRecyclerView.setVisibility(View.GONE);
        if (NetworkDetector.getInstance().isNetworkAvailable(SearchResultActivity.this)) {
            new GetActivitiesByKeywordAsyncTask().execute(keyword);
        }
    }

    class GetActivitiesByKeywordAsyncTask extends AsyncTask<String, Void, Void>
    {
        List<ActivityDescription> searchActivityDescriptionList = new ArrayList<>();
        @Override
        protected Void doInBackground(String... strings)
        {
            String keyword = strings[0];
            HttpRequest request = HttpRequest.get(getString(R.string.get_activities_by_keyword) + keyword);
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        int id = object.getInt("id");
                        String name = object.getString("name");
                        String body = object.getString("body");
                        String imageURL = icollectDomain + "/" + object.getString("image");
                        String gainStartDate = object.getString("gain_start");
                        String gainEndDate = object.getString("gain_end");
                        String exchangeStartDate = object.getString("ex_start");
                        String exchangeEndDate = object.getString("ex_end");

                        ActivityDescription activityDescription = new ActivityDescription();
                        activityDescription.setId(id);
                        activityDescription.setActivityName(name);
                        activityDescription.setActivityContent(body);
                        activityDescription.setActivityImageURL(imageURL);
                        activityDescription.setGainStartDate(gainStartDate);
                        activityDescription.setGainEndDate(gainEndDate);
                        activityDescription.setExchangeStartDate(exchangeStartDate);
                        activityDescription.setExchangeEndDate(exchangeEndDate);
                        searchActivityDescriptionList.add(activityDescription);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            searchResultRecyclerView.setVisibility(View.VISIBLE);
            allFragmentRecycleViewAdapter.addActivities(searchActivityDescriptionList);
        }
    }
}
