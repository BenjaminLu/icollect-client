package com.icollect.client;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.icollect.client.entity.Ticket;
import com.icollect.client.entity.UsedTicketList;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UseQRTicketActivity extends AppCompatActivity
{
    SharedPreferences sp;
    UsedTicketList usedTicketList;
    Ticket ticket;
    JSONObject ticketJson;
    TextView statusMessage;
    ImageView ticketQRImageView;
    BluetoothAdapter bluetoothAdapter;
    UUID uuid;
    String deviceName;
    String deviceAddress;
    boolean receiverIsRegistered;
    AcceptThread acceptThread;
    boolean acceptThreadIsStart = false;
    private BluetoothServerSocket bluetoothServerSocket;
    ConnectedThread connectedThread;
    private BluetoothSocket bluetoothSocket;
    private InputStream instream;
    private OutputStream outstream;
    private static final int TICKET_SUCCESS = 2;
    private static final int TICKET_FAIL = 3;

    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            switch (msg.what) {
                case TICKET_SUCCESS:
                    Bundle b = msg.getData();
                    int tid = b.getInt("tid");
                    String resultMessage = b.getString("message");

                    if (usedTicketList != null) {
                        List<Integer> usedTicketListTicketIds = usedTicketList.getTicketIds();
                        usedTicketListTicketIds.add(tid);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("used_ticket_ids", new Gson().toJson(usedTicketList));
                        editor.commit();
                    } else {
                        usedTicketList = new UsedTicketList();
                        List<Integer> ticketIdList = new ArrayList<>();
                        ticketIdList.add(tid);
                        usedTicketList.setTicketIds(ticketIdList);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("used_ticket_ids", new Gson().toJson(usedTicketList));
                        editor.commit();
                    }
                    Toast.makeText(UseQRTicketActivity.this, usedTicketList.getTicketIds().toString(), Toast.LENGTH_LONG).show();

                    Intent i = new Intent();
                    i.putExtra("used_tid", tid);
                    setResult(RESULT_OK, i);
                    showResultDialog(resultMessage, tid);
                    break;
                case TICKET_FAIL:
                    setResult(RESULT_CANCELED);
                    finish();
                    break;
            }
        }
    };

    private void showResultDialog(String resultMessage, final int tid)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(UseQRTicketActivity.this)
                .setTitle("核銷成功")
                .setMessage("票卷ID : " + tid + "\n" + resultMessage)
                .setPositiveButton("確定", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        try {
                            bluetoothSocket.close();
                            acceptThread.close();
                            if (receiverIsRegistered) {
                                unregisterReceiver(mReceiver);
                                receiverIsRegistered = false;
                            }
                            dialog.dismiss();
                            finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        statusMessage.setText("Bluetooth off");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        statusMessage.setText("Turning Bluetooth off...");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        statusMessage.setText("Bluetooth on");
                        deviceName = bluetoothAdapter.getName();
                        deviceAddress = bluetoothAdapter.getAddress();

                        JSONObject o = new JSONObject();
                        try {
                            o.put("address", deviceAddress);
                            o.put("uuid", uuid.toString());
                            Bitmap QRcodeBitmap = QRCode.from(o.toString()).withCharset("UTF-8").bitmap();
                            ticketQRImageView.setImageBitmap(QRcodeBitmap);
                            acceptThread = new AcceptThread();
                            acceptThread.start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        statusMessage.setText("Turning Bluetooth on...");
                        break;
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_use_qrticket);

        sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        String usedTicketJsonString = sp.getString("used_ticket_ids", null);
        if (usedTicketJsonString != null) {
            usedTicketList = new Gson().fromJson(usedTicketJsonString, UsedTicketList.class);
        }

        String ticketJsonString;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ticketJsonString = extras.getString("ticket");
            this.ticket = new Gson().fromJson(ticketJsonString, Ticket.class);
        }
        String data = ticket.getData();
        String sign = ticket.getSign();
        ticketJson = new JSONObject();
        try {
            ticketJson.put("data", data);
            ticketJson.put("sign", sign);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ticketQRImageView = (ImageView) findViewById(R.id.ticket_qr_image);
        statusMessage = (TextView) findViewById(R.id.status);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        int length = (height > width) ? width : height;
        ticketQRImageView.setMaxWidth(length);
        ticketQRImageView.setMaxHeight(length);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        uuid = UUID.randomUUID();
        if (bluetoothAdapter == null) {
            statusMessage.setText("Not support Bluetooth.");
            Toast.makeText(this, "Not support Bluetooth.", Toast.LENGTH_SHORT);
        } else {
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            registerReceiver(mReceiver, filter);
            receiverIsRegistered = true;
            if (!bluetoothAdapter.isEnabled()) {
                statusMessage.setText("Bluetooth off");
                bluetoothAdapter.enable();
            } else {
                statusMessage.setText("Bluetooth on");
                deviceName = bluetoothAdapter.getName();
                deviceAddress = bluetoothAdapter.getAddress();
                try {
                    JSONObject o = new JSONObject();
                    o.put("address", deviceAddress);
                    o.put("uuid", uuid.toString());
                    Bitmap QRcodeBitmap = QRCode.from(o.toString()).withCharset("UTF-8").bitmap();
                    ticketQRImageView.setImageBitmap(QRcodeBitmap);
                    acceptThread = new AcceptThread();
                    acceptThread.start();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onDestroy()
    {
        if (receiverIsRegistered) {
            unregisterReceiver(mReceiver);
            receiverIsRegistered = false;
        }

        if (acceptThread != null) {
            acceptThread.close();
        }

        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.disable();
        }
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_use_qrticket, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AcceptThread extends Thread
    {
        public void run()
        {
            acceptThreadIsStart = true;
            if (bluetoothAdapter.isEnabled()) {
                try {
                    bluetoothServerSocket = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(deviceName, uuid);
                    if (bluetoothServerSocket != null) {
                        BluetoothSocket socket = bluetoothServerSocket.accept();
                        if (socket != null) {
                            mHandler.post(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    statusMessage.setText("Accept");
                                }
                            });
                            if (socket.isConnected()) {
                                connectedThread = new ConnectedThread(socket);
                                connectedThread.start();
                                acceptThreadIsStart = false;
                                mHandler.post(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        statusMessage.setText("Connected");
                                    }
                                });
                            }
                        }
                    }
                } catch (IOException e) {
                    acceptThreadIsStart = false;
                    e.printStackTrace();
                }
            }

        }

        public void close()
        {
            try {
                if (bluetoothServerSocket != null) {
                    bluetoothServerSocket.close();
                    acceptThreadIsStart = false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class ConnectedThread extends Thread
    {
        boolean isSuccess = false;

        public ConnectedThread(BluetoothSocket socket)
        {
            bluetoothSocket = socket;
            mHandler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    statusMessage.setText("Connecting");
                }
            });
        }

        public void run()
        {
            int bufferSize = 10240;
            byte[] buffer = new byte[bufferSize];  // buffer store for the stream
            int bytesSize = -1;
            String response = null;
            isSuccess = false;
            try {
                instream = bluetoothSocket.getInputStream();
                outstream = bluetoothSocket.getOutputStream();

                outstream.write(ticketJson.toString().getBytes("UTF-8"));
                bytesSize = instream.read(buffer);
                response = new String(buffer, "UTF-8");

                JSONObject o = new JSONObject(response);
                boolean status = o.getBoolean("status");
                final String resultMessage = o.getString("message");
                if (status) {
                    int tid = o.getInt("tid");
                    Bundle b = new Bundle();
                    b.putInt("tid", tid);
                    b.putString("message", resultMessage);
                    Message m = new Message();
                    m.what = TICKET_SUCCESS;
                    m.setData(b);
                    mHandler.sendMessage(m);
                } else {
                    mHandler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            statusMessage.setText(resultMessage);
                        }
                    });
                    Message m = new Message();
                    m.what = TICKET_FAIL;
                    mHandler.sendMessage(m);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void write(byte[] bytes)
        {
            try {
                outstream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void cancel()
        {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
