package com.icollect.client.events;

/**
 * Created by Administrator on 2015/10/28.
 */
public class LeaveActivityEvent
{
    int activityId;

    public LeaveActivityEvent(int activityId)
    {
        this.activityId = activityId;
    }

    public int getActivityId()
    {
        return activityId;
    }

    public void setActivityId(int activityId)
    {
        this.activityId = activityId;
    }
}
