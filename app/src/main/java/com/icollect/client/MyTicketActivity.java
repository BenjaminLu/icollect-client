package com.icollect.client;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.icollect.client.adapter.ActivityListOfTicketsRecyclerViewAdapter;
import com.icollect.client.api.WebAPI;
import com.icollect.client.entity.ActivityDescription;
import com.icollect.client.entity.Ticket;
import com.icollect.client.entity.UsedTicketList;
import com.icollect.client.itemanimator.CustomItemAnimator;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class MyTicketActivity extends AppCompatActivity
{
    private int TICKET_DISABLE_RESULT = 5;
    SharedPreferences sp;
    String mSession;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Toolbar toolbar;
    Spinner spinner;
    ActivityListOfTicketsRecyclerViewAdapter activityListOfTicketsRecyclerViewAdapter;
    String firstSpinnerText = "全部";
    ArrayList<Ticket> tickets = new ArrayList<>();
    ArrayList<ActivityDescription> activites;
    String[] activityNames;
    ArrayAdapter activityAdapter;
    UsedTicketList usedTicketList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ticket);

        EventBus.getDefault().register(this);
        sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        mSession = sp.getString("session", null);
        usedTicketList = new Gson().fromJson(sp.getString("used_ticket_ids", null), UsedTicketList.class);

        recyclerView = (RecyclerView) findViewById(R.id.tickets_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new CustomItemAnimator());
        activityListOfTicketsRecyclerViewAdapter = new ActivityListOfTicketsRecyclerViewAdapter(new ArrayList<Ticket>(), R.layout.partial_activities_of_tickets_row, MyTicketActivity.this);
        if (usedTicketList != null) {
            activityListOfTicketsRecyclerViewAdapter.setUsedTicketIds(usedTicketList.getTicketIds());
        }
        recyclerView.setAdapter(activityListOfTicketsRecyclerViewAdapter);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_tickets);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.theme_accent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                swipeRefreshLayout.setRefreshing(true);
                WebAPI.getInstance().getUserTickets(mSession);
                activites = WebAPI.getInstance().getUserActivityOfTickets(mSession);

                if (activites == null) {
                    activites = new ArrayList<ActivityDescription>();
                }

                if (activites.size() > 0) {
                    activityNames = new String[(activites.size() + 1)];
                    activityNames[0] = firstSpinnerText;
                    for (int i = 1; i < (activites.size() + 1); i++) {
                        activityNames[i] = activites.get(i - 1).getActivityName();
                    }
                }

                if (activityNames == null) {
                    activityNames = new String[0];
                }

                activityAdapter = new ArrayAdapter<String>(MyTicketActivity.this, R.layout.partial_my_ticket_spinner_text, activityNames);
                spinner.setAdapter(activityAdapter);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        spinner = (Spinner) findViewById(R.id.activity_spinner);
        activites = WebAPI.getInstance().getUserActivityOfTickets(mSession);
        if (activites == null) {
            activites = new ArrayList<ActivityDescription>();
        }

        if (activites.size() > 0) {
            activityNames = new String[(activites.size() + 1)];
            activityNames[0] = firstSpinnerText;
            for (int i = 1; i < (activites.size() + 1); i++) {
                activityNames[i] = activites.get(i - 1).getActivityName();
            }
        }

        if (activityNames == null) {
            activityNames = new String[0];
        }

        activityAdapter = new ArrayAdapter<String>(MyTicketActivity.this, R.layout.partial_my_ticket_spinner_text, activityNames);
        spinner.setAdapter(activityAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position == 0 && tickets != null && tickets.size() > 0) {
                    ArrayList<Ticket> clonedTickets = cloneTickets(tickets);
                    activityListOfTicketsRecyclerViewAdapter.clearTickets();
                    activityListOfTicketsRecyclerViewAdapter.addTickets(clonedTickets);
                    SnackbarManager.show(
                            Snackbar.with(getApplicationContext())
                                    .type(SnackbarType.SINGLE_LINE)
                                    .position(Snackbar.SnackbarPosition.BOTTOM)
                                    .text("共" + clonedTickets.size() + "張票卷")
                                    .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                                    .animation(true)
                            , MyTicketActivity.this);
                } else if (position > 0) {
                    position--;
                    int aid = activites.get(position).getId();
                    ArrayList<Ticket> tickets = findTicketsOfActivity(aid);
                    activityListOfTicketsRecyclerViewAdapter.clearTickets();
                    activityListOfTicketsRecyclerViewAdapter.addTickets(tickets);
                    SnackbarManager.show(
                            Snackbar.with(getApplicationContext())
                                    .type(SnackbarType.SINGLE_LINE)
                                    .position(Snackbar.SnackbarPosition.BOTTOM)
                                    .text("共" + tickets.size() + "張票卷")
                                    .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                                    .animation(true)
                            , MyTicketActivity.this);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        WebAPI.getInstance().getUserTickets(mSession);
    }

    public void startUseTicketActivity(Ticket ticket)
    {
        Intent i = new Intent(MyTicketActivity.this, UseQRTicketActivity.class);
        i.putExtra("ticket", new Gson().toJson(ticket));
        startActivityForResult(i, TICKET_DISABLE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == TICKET_DISABLE_RESULT && resultCode == RESULT_OK && data != null) {
            int tid = data.getExtras().getInt("used_tid");
            if (tid > 0) {
                activityListOfTicketsRecyclerViewAdapter.addUsedTicketId(tid);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_ticket, menu);
        return true;
    }

    public void onEvent(ArrayList<Ticket> newTickets)
    {
        activityListOfTicketsRecyclerViewAdapter.clearTickets();
        this.tickets = newTickets;
        activityListOfTicketsRecyclerViewAdapter.addTickets(newTickets);
        SnackbarManager.show(
                Snackbar.with(getApplicationContext())
                        .type(SnackbarType.SINGLE_LINE)
                        .position(Snackbar.SnackbarPosition.BOTTOM)
                        .text("共" + newTickets.size() + "張票卷")
                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                        .animation(true)
                , MyTicketActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.search_ticket:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this).setTitle("搜尋");
                final EditText editText = new EditText(MyTicketActivity.this);
                alertDialog.setView(editText);
                alertDialog.setNegativeButton("名稱", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        ArrayList<Ticket> tickets = findTicketsByName(editText.getText().toString());
                        activityListOfTicketsRecyclerViewAdapter.clearTickets();
                        activityListOfTicketsRecyclerViewAdapter.addTickets(tickets);
                        SnackbarManager.show(
                                Snackbar.with(getApplicationContext())
                                        .type(SnackbarType.SINGLE_LINE)
                                        .position(Snackbar.SnackbarPosition.BOTTOM)
                                        .text("共" + tickets.size() + "張票卷")
                                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                                        .animation(true)
                                , MyTicketActivity.this);
                        dialog.dismiss();
                    }
                }).setNeutralButton("內容", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        ArrayList<Ticket> tickets = findTicketsByBody(editText.getText().toString());
                        activityListOfTicketsRecyclerViewAdapter.clearTickets();
                        activityListOfTicketsRecyclerViewAdapter.addTickets(tickets);
                        SnackbarManager.show(
                                Snackbar.with(getApplicationContext())
                                        .type(SnackbarType.SINGLE_LINE)
                                        .position(Snackbar.SnackbarPosition.BOTTOM)
                                        .text("共" + tickets.size() + "張票卷")
                                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                                        .animation(true)
                                , MyTicketActivity.this);
                        dialog.dismiss();
                    }
                }).create();
                alertDialog.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public static ArrayList<Ticket> cloneTickets(ArrayList<Ticket> list)
    {
        ArrayList<Ticket> clone = new ArrayList<Ticket>(list.size());
        for (Ticket ticket : list) {
            clone.add(ticket.clone());
        }
        return clone;
    }

    public ArrayList<Ticket> findTicketsOfActivity(int aid)
    {
        ArrayList<Ticket> ticketsOfActivity = new ArrayList<>();
        for (int i = 0; i < this.tickets.size(); i++) {
            Ticket ticket = this.tickets.get(i);
            if (ticket.getActivityID() == aid) {
                ticketsOfActivity.add(ticket);
            }
        }
        return ticketsOfActivity;
    }

    public ArrayList<Ticket> findTicketsByName(String name)
    {
        ArrayList<Ticket> ticketsOfActivity = new ArrayList<>();
        for (int i = 0; i < this.tickets.size(); i++) {
            Ticket ticket = this.tickets.get(i);
            if (ticket.getName().indexOf(name) >= 0) {
                ticketsOfActivity.add(ticket);
            }
        }
        return ticketsOfActivity;
    }

    public ArrayList<Ticket> findTicketsByBody(String body)
    {
        ArrayList<Ticket> ticketsOfActivity = new ArrayList<>();
        for (int i = 0; i < this.tickets.size(); i++) {
            Ticket ticket = this.tickets.get(i);
            if (ticket.getBody().indexOf(body) >= 0) {
                ticketsOfActivity.add(ticket);
            }
        }
        return ticketsOfActivity;
    }
}
