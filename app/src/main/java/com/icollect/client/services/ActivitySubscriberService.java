package com.icollect.client.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import com.icollect.client.MainActivity;
import com.icollect.client.R;
import com.icollect.client.events.LeaveActivityEvent;
import com.icollect.client.events.WatchingActivityEvent;
import com.icollect.client.helpers.UserIdHelper;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import de.greenrobot.event.EventBus;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Administrator on 2015/10/9.
 */
public class ActivitySubscriberService extends Service
{
    Socket socket;
    NotificationManagerCompat notificationManager;
    int maxNotificationNumber = 10;
    int notificationNumber = 0;
    int id;

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        EventBus.getDefault().register(this);
        id = UserIdHelper.getInstance().getUserId();
        notificationManager = NotificationManagerCompat.from(getApplicationContext());
        try {
            IO.Options options = new IO.Options();
            options.forceNew = true;
            options.reconnection = true;
            socket = IO.socket("http://icollect.tw:3000", options);
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {

                }
            }).on("new-activity-published", new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                    new LoadImageAsyncTask().execute((String) args[0]);
                }
            }).on("received-gift-" + id, new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                    String message = (String) args[0];
                    new ReceivedGiftNotificationAsyncTask().execute(message);
                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                }
            });
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void onEventAsync(WatchingActivityEvent event)
    {
        socket.emit("mobile.user.watch.activity", event.getActivityId());
    }

    public void onEventAsync(LeaveActivityEvent event)
    {
        socket.emit("mobile.user.leave.activity", event.getActivityId());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (socket != null) {
            socket.disconnect();
        }
    }

    class ReceivedGiftNotificationAsyncTask extends AsyncTask<String, Void, Void>
    {
        @Override
        protected Void doInBackground(String... params)
        {
            String message = params[0];
            System.out.println(message);
            try {
                JSONObject o = new JSONObject(message);
                String sender = o.getString("sender");
                int ticketNumber = o.getInt("ticket_number");
                String image = o.getString("activity_img");
                String imageUrl = getApplicationContext().getString(R.string.icollect_domain) + "/" + image;
                String giftMessage = o.getString("message");

                notificationNumber = notificationNumber % 10;
                NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
                if (giftMessage != null) {
                    bigPictureStyle.setSummaryText(giftMessage);
                }

                Bitmap imageBitmap = ImageLoader.getInstance().loadImageSync(imageUrl);
                if (imageBitmap != null) {
                    bigPictureStyle.bigPicture(imageBitmap);
                }

                Notification notification = new NotificationCompat.Builder(getApplicationContext())
                        .setAutoCancel(true)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(sender + "送給了您" + ticketNumber + "張票券禮物")
                        .setContentText(giftMessage)
                        .setStyle(bigPictureStyle)
                        .setLargeIcon(imageBitmap)
                        .setPriority(Notification.PRIORITY_MAX)
                        .build();
                notificationManager.notify(notificationNumber, notification);
                notificationNumber++;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    class LoadImageAsyncTask extends AsyncTask<String, Void, Void>
    {
        int id;
        String url;
        String imageUrl;
        String activityName;
        String activityBody;
        Bitmap image;

        @Override
        protected Void doInBackground(String... params)
        {
            String infoJson = params[0];
            try {
                JSONObject o = new JSONObject(infoJson);
                id = o.getInt("activity_id");
                url = o.getString("url");
                imageUrl = o.getString("image");
                activityName = o.getString("name");
                activityBody = o.getString("body");
                image = ImageLoader.getInstance().loadImageSync(imageUrl);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args)
        {
            notificationNumber = notificationNumber % 10;
            NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
            if (activityBody != null) {
                bigPictureStyle.setSummaryText(activityBody);
            }

            if (image != null) {
                bigPictureStyle.bigPicture(image);
            }

            Intent intent = new Intent(ActivitySubscriberService.this, MainActivity.class);
            Bundle b = new Bundle();
            b.putInt("activity_id", id);
            b.putString("url", url);
            b.putString("image_url", imageUrl);
            b.putString("activity_name", activityName);
            b.putString("activity_body", activityBody);
            intent.putExtras(b);
            PendingIntent pendingIntent = PendingIntent.getActivity(ActivitySubscriberService.this, 0, intent, 0);

            Notification notification = new NotificationCompat.Builder(getApplicationContext())
                    .setAutoCancel(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(activityName)
                            //close title
                    .setContentText(activityBody)
                    .setStyle(bigPictureStyle)
                    .setLargeIcon(image)
                    .setContentIntent(pendingIntent)
                    .setPriority(Notification.PRIORITY_MAX)
                    .build();
            notificationManager.notify(notificationNumber, notification);
            notificationNumber++;
        }
    }
}
