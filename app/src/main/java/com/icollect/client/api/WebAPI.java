package com.icollect.client.api;

import android.os.AsyncTask;

import com.icollect.client.entity.ActivityDescription;
import com.icollect.client.entity.Ticket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/9/23.
 */
public class WebAPI
{
    private static final String domain = "http://icollect.tw/";
    private static WebAPI api = new WebAPI();

    public static WebAPI getInstance()
    {
        return api;
    }

    public ArrayList<Ticket> getUserTickets(String sessionCookie)
    {
        GetUserTicketsAsyncTask task = new GetUserTicketsAsyncTask();
        task.execute(sessionCookie);
        return null;
    }

    class GetUserTicketsAsyncTask extends AsyncTask<String, Void, ArrayList<Ticket>>
    {
        @Override
        protected ArrayList<Ticket> doInBackground(String[] params)
        {
            String sessionCookie = params[0];
            HttpRequest request = HttpRequest.get("http://icollect.tw/api/ticket").header("Cookie", sessionCookie);
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONArray array = new JSONArray(response);
                    ArrayList<Ticket> tickets = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject o = array.getJSONObject(i);
                        String data = o.getString("data");
                        String sign = o.getString("sign");

                        JSONObject ticketJson = new JSONObject(data);
                        int id = ticketJson.getInt("id");
                        String name = ticketJson.getString("name");
                        String body = ticketJson.getString("body");
                        int price = ticketJson.getInt("price");
                        String image = ticketJson.getString("image");
                        image = domain + image;
                        int activityID = ticketJson.getInt("activity_id");
                        int userID = ticketJson.getInt("user_id");
                        boolean status = ticketJson.getBoolean("status");
                        String createAt = ticketJson.getString("created_at");
                        String updateAt = ticketJson.getString("updated_at");

                        Ticket ticket = new Ticket();
                        ticket.setId(id);
                        ticket.setName(name);
                        ticket.setBody(body);
                        ticket.setPrice(price);
                        ticket.setImage(image);
                        ticket.setActivityID(activityID);
                        ticket.setUserID(userID);
                        ticket.setStatus(status);
                        ticket.setCreateAt(createAt);
                        ticket.setUpdateAt(updateAt);
                        ticket.setData(data);
                        ticket.setSign(sign);
                        tickets.add(ticket);
                    }
                    return tickets;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Ticket> tickets)
        {
            if (tickets != null && tickets.size() > 0) {
                EventBus.getDefault().post(tickets);
            }
        }
    }

    public ArrayList<ActivityDescription> getUserActivityOfTickets(String sessionCookie)
    {
        GetUserActivityOfTicketsAsyncTask task = new GetUserActivityOfTicketsAsyncTask();
        try {
            ArrayList<ActivityDescription> activities = (ArrayList<ActivityDescription>) task.execute(sessionCookie).get();
            return activities;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    class GetUserActivityOfTicketsAsyncTask
            extends AsyncTask<String, Void, ArrayList<ActivityDescription>>
    {
        @Override
        protected ArrayList<ActivityDescription> doInBackground(String[] params)
        {
            String sessionCookie = params[0];
            HttpRequest request = HttpRequest.get("http://icollect.tw/api/ticket/activity/list").header("Cookie", sessionCookie);
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONArray array = new JSONArray(response);
                    ArrayList<ActivityDescription> activities = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject o = array.getJSONObject(i);
                        int id = o.getInt("id");
                        String name = o.getString("name");
                        String body = o.getString("body");
                        ActivityDescription activity = new ActivityDescription();
                        activity.setId(id);
                        activity.setActivityName(name);
                        activity.setActivityContent(body);
                        activities.add(activity);
                    }
                    return activities;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
